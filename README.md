Instalación de Openvas
==============


OpenVAS (Open Vulnerability Assessment System,1​ inicialmente denominado GNessUs), es una suite de software, que ofrece un marco de trabajo para integrar servicios y herramientas especializadas en el escaneo y gestión de vulnerabilidades de seguridad de sistemas informáticos.


|    Version      | Tag     | Puerto Web  |
|-----------------|---------|-------------|
| 9               | latest/9| 443         |


Uso
-----
1. Edite el archivo docker-compose.yml definiendo la clave del sistema a través de la variable OV_PASSWORD
Simplemente ejecute:

```
docker-compose up -d

```

#### Soporte de volúmenes
Dentro de su carpeta de openvas existirá la carpeta data la cuál será montada dentro del contenedor en el directorio `/var/lib/openvas/mgr/`:

El directorio debe existir antes de correr el comando de instalación. Esta carpeta contiene los datos y configuraciones del servicio.

#### Crear usuario administrador
```
docker exec -it openvas bash
openvasmd --create-user admin
```

#### Cambiar contraseña de administrador
Para cambiar la contraseña sólo debe cambiar la variable de entorno `OV_PASSWORD`:
```
docker exec -it openvas bash
openvasmd --user=admin --new-password=new_password
```

#### Actualizar reglas del sistema
Para actualizar las reglas de openvas usted puede ejecutar los siguientes comandos dentro del contenedor. Así:
```
docker exec -it openvas bash
## inside container
greenbone-nvt-sync
openvasmd --rebuild --progress
greenbone-certdata-sync
greenbone-scapdata-sync
openvasmd --update --verbose --progress

/etc/init.d/openvas-manager restart
/etc/init.d/openvas-scanner restart
```
